package simulation;

import core.EmergencyService;
import core.ResourceProvider;
import engine.DefaultEngine;
import events.physician.AddingPhysicianEvent;
import events.physician.ReleasingPhysicianEvent;
import events.room.AddingRoomEvent;
import events.room.ReleasingRoomEvent;
import interfaces.Engine;
import interfaces.Service;
import interfaces.Simulation;
import messagesManagement.ThreadOrdering;
import orders.OrderType;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class DefaultSimulation implements Simulation {
    private final DefaultEngine engine;
    private final List<EmergencyService> services;
    private final ResourceProvider provider;
    private final ThreadOrdering threadOrdering;

    public DefaultSimulation(Clock clock, int nbPatients, int nbNurses, int nbPhysicians, int nbRooms, int nbServices) throws Exception {
        this.engine = new DefaultEngine(clock);
        services = new ArrayList<>();
        this.provider = new ResourceProvider(engine);
        this.threadOrdering = new ThreadOrdering();

        for (int i = 0; i < nbServices; i++) {
            this.services.add(new EmergencyService(engine, i, nbPatients, nbNurses, nbPhysicians, nbRooms));
        }
    }

    public DefaultSimulation() throws Exception {
        this(Clock.systemDefaultZone(), 100, 5, 2, 20, 1);
    }

    @Override
    public String toString() {
        String output = "";
        for (int i = 0; i < services.size(); i++) {
            output += "-------------\n" + services.get(i).toString();
        }
        return output;
    }

    @Override
    public void update() {
        engine.update();
    }

    @Override
    public Engine getEngine() {
        return this.engine;
    }

    @Override
    public List<EmergencyService> getServices() {
        return this.services;
    }

    @Override
    public void executeOrder(OrderType order, int serviceIndex) {
        if (serviceIndex >= services.size()) {
            threadOrdering.addMessage("Service doesn't exist: " + serviceIndex);
            return;
        }
        Service service = services.get(serviceIndex);
        if (order == OrderType.AddPhysician) {
            this.engine.register(new AddingPhysicianEvent((EmergencyService) service, provider));
        } else if (order == OrderType.AddRoom) {
            this.engine.register(new AddingRoomEvent((EmergencyService) service, provider));
        } else if (order == OrderType.ReleasePhysician) {
            this.engine.register(new ReleasingPhysicianEvent((EmergencyService) service, provider, threadOrdering));
        } else if (order == OrderType.ReleaseRoom) {
            this.engine.register(new ReleasingRoomEvent((EmergencyService) service, provider, threadOrdering));
        }
    }

    public Vector extractMessagesList() {
        return this.threadOrdering.extractMessagesList();
    }

    public void addMessage(String... messages) {
        this.threadOrdering.addMessage(messages);
    }

}
