package events.patient;

import core.EmergencyService;
import engine.DefaultEngine;
import events.physician.TreatingPatientEvent;
import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.patient.PatientStates;
import resources.physician.Physician;
import resources.room.Room;

import java.time.Duration;

import static utils.Durations.WAITING_FOR_PHYSICIAN_TIME;

public class WaitingForPhysicianEvent implements Event {
    private final Resource<Patient> patient;
    private final DefaultEngine engine;
    private final EmergencyService service;
    private final Resource<Room> room;
    private final Duration duration;

    public WaitingForPhysicianEvent(DefaultEngine engine, EmergencyService service, Resource<Room> room, Resource<Patient> patient) {
        this.patient = patient;
        this.engine = engine;
        this.service = service;
        this.room = room;
        this.duration = Duration.ofSeconds(WAITING_FOR_PHYSICIAN_TIME);
    }

    @Override
    public void trigger() throws Exception {
        Resource<Physician> physician = service.reserveResource(Physician.class.getSimpleName());
        if (physician == null) {
            engine.register(new WaitingForPhysicianEvent(engine, service, room, patient));
            return;
        }
        service.incrementPromisedPhysician();
        patient.setState(PatientStates.BeingTreated);
        engine.register(new TreatingPatientEvent(engine, service, room, physician, patient));
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
