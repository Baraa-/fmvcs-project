package events.patient;

import core.EmergencyService;
import engine.DefaultEngine;
import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.patient.PatientStates;

import java.time.Duration;

import static utils.Durations.ADMISSION_TIME;

public class AdmissionEvent implements Event {
    private final Duration duration;
    private final Resource<Patient> patient;
    private final DefaultEngine engine;
    private final EmergencyService service;

    public AdmissionEvent(DefaultEngine engine, EmergencyService service, Resource<Patient> patient) {
        this.duration = Duration.ofSeconds(ADMISSION_TIME);
        this.patient = patient;
        this.engine = engine;
        this.service = service;
    }

    @Override
    public void trigger() throws Exception {
        patient.setState(PatientStates.Admitted);
        engine.register(new FillingPaperworkEvent(engine, service, patient));
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
