package events.patient;

import core.EmergencyService;
import engine.DefaultEngine;
import events.nurse.ProcessingPaperworkEvent;
import interfaces.Event;
import resources.Resource;
import resources.nurse.Nurse;
import resources.patient.Patient;
import resources.patient.PatientStates;
import utils.Random;

import java.time.Duration;

import static utils.Durations.MAX_FILLING_PAPERWORK_TIME;
import static utils.Durations.MIN_FILLING_PAPERWORK_TIME;

public class FillingPaperworkEvent implements Event {
    private final Resource<Patient> patient;
    private final DefaultEngine engine;
    private final EmergencyService service;
    private final Duration duration;

    public FillingPaperworkEvent(DefaultEngine engine, EmergencyService service, Resource<Patient> patient) {
        this.patient = patient;
        this.engine = engine;
        this.service = service;
        this.duration = Random.getRandomDuration(MIN_FILLING_PAPERWORK_TIME, MAX_FILLING_PAPERWORK_TIME);
    }

    @Override
    public void trigger() throws Exception {
        Resource<Nurse> nurse = service.reserveResource(Nurse.class.getSimpleName());
        if (nurse == null) {
            engine.register(new FillingPaperworkEvent(engine, service, patient));
            return;
        }
        patient.setState(PatientStates.Waiting);
        engine.register(new ProcessingPaperworkEvent(engine, service, nurse, patient));
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
