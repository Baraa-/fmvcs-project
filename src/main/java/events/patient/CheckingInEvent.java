package events.patient;

import core.EmergencyService;
import engine.DefaultEngine;
import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.patient.PatientStates;
import utils.Random;

import java.time.Duration;
import java.time.Instant;

import static utils.Durations.*;

public class CheckingInEvent implements Event {
    private final Resource<Patient> patient;
    private final DefaultEngine engine;
    private final EmergencyService service;
    private Duration duration;
    private Instant arrivedInstant;

    public CheckingInEvent(DefaultEngine engine, EmergencyService service, Resource<Patient> patient) {
        this.engine = engine;
        this.duration = Random.getRandomDuration(MIN_ARRIVAL_TIME, MAX_ARRIVAL_TIME);
        this.patient = patient;
        this.service = service;
        this.arrivedInstant = null;
    }

    @Override
    public void trigger() throws Exception {
        if (arrivedInstant == null) {
            this.arrivedInstant = engine.getCurrentInstant();
        }
        patient.setState(PatientStates.CheckedIn);
        Instant nextAvailability = service.computeProvisionalResourceAvailabilityInstant();
        if (nextAvailability.compareTo(arrivedInstant.plus(Duration.ofSeconds(MAX_WAITING_TIME))) <= 0) {
            if (service.getPromisedRooms() > 0 && service.getPromisedPhysicians() > 0) {

                service.consumePromisedResources();
                engine.register(new AdmissionEvent(engine, service, patient));
            } else {
                CheckingInEvent event = new CheckingInEvent(engine, service, patient);
                event.setDuration(Duration.ofSeconds(1));
                event.setArrivedInstant(arrivedInstant);
                engine.register(event);
            }
        } else {
        /*System.out.println("The patient #" + patient.getId()
                + " Arrival time: " + this.arrivedInstant
                + ", Waiting time: " + Duration.between(arrivedInstant, nextAvailability).getSeconds()
                + "s (MAX_WAITING_TIME: " + MAX_WAITING_TIME +"s)"); //TODO*/
            engine.register(new LeavingEvent(patient));
        }
    }

    @Override
    public Duration getDuration() {
        return this.duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public void setArrivedInstant(Instant instant) {
        this.arrivedInstant = instant;
    }

}
