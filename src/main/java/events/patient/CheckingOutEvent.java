package events.patient;

import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.patient.PatientStates;

import java.time.Duration;

import static utils.Durations.CHECKING_OUT_TIME;

public class CheckingOutEvent implements Event {
    private final Resource<Patient> patient;
    private final Duration duration;

    public CheckingOutEvent(Resource<Patient> patient) {
        this.patient = patient;
        this.duration = Duration.ofSeconds(CHECKING_OUT_TIME);
    }

    @Override
    public void trigger() throws Exception {
        patient.setState(PatientStates.CheckedOut);
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
