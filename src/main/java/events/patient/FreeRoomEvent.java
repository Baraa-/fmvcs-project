package events.patient;

import engine.DefaultEngine;
import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.room.Room;
import resources.room.RoomStates;
import utils.Random;

import java.time.Duration;

import static utils.Durations.MAX_FREE_ROOM_TIME;
import static utils.Durations.MIN_FREE_ROOM_TIME;

public class FreeRoomEvent implements Event {
    private final Resource<Patient> patient;
    private final DefaultEngine engine;
    private final Resource<Room> room;
    private final Duration duration;

    public FreeRoomEvent(DefaultEngine engine, Resource<Room> room, Resource<Patient> patient) {
        this.patient = patient;
        this.engine = engine;
        this.room = room;
        this.duration = Random.getRandomDuration(MIN_FREE_ROOM_TIME, MAX_FREE_ROOM_TIME);
    }

    @Override
    public void trigger() throws Exception {
        room.setState(RoomStates.Free);
        engine.register(new CheckingOutEvent(patient));
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
