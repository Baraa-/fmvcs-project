package events.patient;

import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.patient.PatientStates;

import java.time.Duration;

import static utils.Durations.LEAVING_TIME;

public class LeavingEvent implements Event {
    private final Resource<Patient> patient;
    private final Duration duration;

    public LeavingEvent(Resource<Patient> patient) {
        this.patient = patient;
        this.duration = Duration.ofSeconds(LEAVING_TIME);
    }

    @Override
    public void trigger() throws Exception {
        this.patient.setState(PatientStates.Left);
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
