package events.patient;

import core.EmergencyService;
import engine.DefaultEngine;
import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.patient.PatientStates;
import resources.room.Room;

import java.time.Duration;

import static utils.Durations.WAITING_FOR_ROOM_TIME;

public class WaitingForRoomEvent implements Event {
    private final Resource<Patient> patient;
    private final DefaultEngine engine;
    private final EmergencyService service;
    private final Duration duration;

    public WaitingForRoomEvent(DefaultEngine engine, EmergencyService service, Resource<Patient> patient) {
        this.patient = patient;
        this.engine = engine;
        this.service = service;
        this.duration = Duration.ofSeconds(WAITING_FOR_ROOM_TIME);
    }

    @Override
    public void trigger() throws Exception {
        Resource<Room> room = service.reserveResource(Room.class.getSimpleName());
        if (room == null) {
            engine.register(new WaitingForRoomEvent(engine, service, patient));
            return;
        }
        service.incrementPromisedRoom();
        patient.setState(PatientStates.ReadyForTreatment);
        engine.register(new WaitingForPhysicianEvent(engine, service, room, patient));
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
