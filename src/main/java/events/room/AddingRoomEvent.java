package events.room;

import core.EmergencyService;
import core.ResourceProvider;
import interfaces.Event;
import resources.Resource;
import resources.physician.Physician;
import resources.room.Room;

import java.time.Duration;

import static utils.Durations.ADDING_ROOM_TIME;

public class AddingRoomEvent implements Event {
    private final Duration duration;
    private final EmergencyService service;
    private final ResourceProvider provider;

    public AddingRoomEvent(EmergencyService service, ResourceProvider provider) {
        this.service = service;
        this.provider = provider;
        this.duration = Duration.ofSeconds(ADDING_ROOM_TIME);
    }

    @Override
    public void trigger() throws Exception {
        Resource<Physician> resource = this.provider.askForResource(Room.class.getSimpleName());
        if (resource != null) {
            this.service.addResource(resource);
        }
    }

    @Override
    public Duration getDuration() {
        return this.duration;
    }
}
