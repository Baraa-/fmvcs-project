package events.room;

import core.EmergencyService;
import core.ResourceProvider;
import interfaces.Event;
import messagesManagement.ThreadOrdering;
import resources.Resource;
import resources.room.Room;

import java.time.Duration;

import static utils.Durations.RELEASING_ROOM_TIME;

public class ReleasingRoomEvent implements Event {
    private final Duration duration;
    private final EmergencyService service;
    private final ResourceProvider provider;
    private final ThreadOrdering threadOrdering;

    public ReleasingRoomEvent(EmergencyService service, ResourceProvider provider, ThreadOrdering threadOrdering) {
        this.service = service;
        this.provider = provider;
        this.duration = Duration.ofSeconds(RELEASING_ROOM_TIME);
        this.threadOrdering = threadOrdering;
    }

    @Override
    public void trigger() throws Exception {
        Resource resource = this.service.giveExistingResource(Room.class.getSimpleName());
        if (resource != null) {
            provider.assimilateResource(resource);
        } else {
            threadOrdering.addMessage("You can't release a room while there are patients in the hospital");
        }
    }

    @Override
    public Duration getDuration() {
        return this.duration;
    }
}
