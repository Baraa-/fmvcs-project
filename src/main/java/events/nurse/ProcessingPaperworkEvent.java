package events.nurse;

import core.EmergencyService;
import engine.DefaultEngine;
import events.patient.WaitingForRoomEvent;
import interfaces.Event;
import resources.Resource;
import resources.nurse.Nurse;
import resources.nurse.NurseStates;
import resources.patient.Patient;
import utils.Random;

import java.time.Duration;

import static utils.Durations.MAX_PROCESSING_PAPERWORK_TIME;
import static utils.Durations.MIN_PROCESSING_PAPERWORK_TIME;

public class ProcessingPaperworkEvent implements Event {
    private final DefaultEngine engine;
    private final Resource<Nurse> nurse;
    private final Resource<Patient> patient;
    private final EmergencyService service;
    private final Duration duration;

    public ProcessingPaperworkEvent(DefaultEngine engine, EmergencyService service, Resource<Nurse> nurse, Resource<Patient> patient) {
        this.engine = engine;
        this.nurse = nurse;
        this.patient = patient;
        this.service = service;
        this.duration = Random.getRandomDuration(MIN_PROCESSING_PAPERWORK_TIME, MAX_PROCESSING_PAPERWORK_TIME);
    }

    @Override
    public void trigger() throws Exception {
        nurse.setState(NurseStates.Available);
        engine.register(new WaitingForRoomEvent(engine, service, patient));
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
