package events.physician;

import core.EmergencyService;
import engine.DefaultEngine;
import events.patient.FreeRoomEvent;
import interfaces.Event;
import resources.Resource;
import resources.patient.Patient;
import resources.patient.PatientStates;
import resources.physician.Physician;
import resources.physician.PhysicianStates;
import resources.room.Room;
import utils.Random;

import java.time.Duration;

import static utils.Durations.MAX_TREATING_PATIENT_TIME;
import static utils.Durations.MIN_TREATING_PATIENT_TIME;

public class TreatingPatientEvent implements Event {
    private final Resource<Patient> patient;
    private final DefaultEngine engine;
    private final EmergencyService service;
    private final Resource<Room> room;
    private final Resource<Physician> physician;
    private final Duration duration;

    public TreatingPatientEvent(DefaultEngine engine, EmergencyService service, Resource<Room> room, Resource<Physician> physician, Resource<Patient> patient) {
        this.patient = patient;
        this.engine = engine;
        this.service = service;
        this.room = room;
        this.physician = physician;
        this.duration = Random.getRandomDuration(MIN_TREATING_PATIENT_TIME, MAX_TREATING_PATIENT_TIME);
    }

    @Override
    public void trigger() throws Exception {
        patient.setState(PatientStates.Treated);
        physician.setState(PhysicianStates.Available);
        engine.register(new FreeRoomEvent(engine, room, patient));
    }

    @Override
    public Duration getDuration() {
        return duration;
    }
}
