package events.physician;

import core.EmergencyService;
import core.ResourceProvider;
import interfaces.Event;
import messagesManagement.ThreadOrdering;
import resources.Resource;
import resources.physician.Physician;

import java.time.Duration;

import static utils.Durations.RELEASING_PHYSICIAN_TIME;

public class ReleasingPhysicianEvent implements Event {
    private final Duration duration;
    private final EmergencyService service;
    private final ResourceProvider provider;
    private final ThreadOrdering threadOrdering;

    public ReleasingPhysicianEvent(EmergencyService service, ResourceProvider provider, ThreadOrdering threadOrdering) {
        this.service = service;
        this.provider = provider;
        this.duration = Duration.ofSeconds(RELEASING_PHYSICIAN_TIME);
        this.threadOrdering = threadOrdering;
    }

    @Override
    public void trigger() throws Exception {
        Resource resource = this.service.giveExistingResource(Physician.class.getSimpleName());
        if (resource != null) {
            provider.assimilateResource(resource);
        } else {
            threadOrdering.addMessage("You can't release a physician while there are patients in the hospital");
        }
    }

    @Override
    public Duration getDuration() {
        return this.duration;
    }
}
