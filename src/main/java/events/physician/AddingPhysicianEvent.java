package events.physician;

import core.EmergencyService;
import core.ResourceProvider;
import interfaces.Event;
import resources.Resource;
import resources.physician.Physician;

import java.time.Duration;

import static utils.Durations.ADDING_PHYSICIAN_TIME;

public class AddingPhysicianEvent implements Event {
    private final Duration duration;
    private final EmergencyService service;
    private final ResourceProvider provider;

    public AddingPhysicianEvent(EmergencyService service, ResourceProvider provider) {
        this.service = service;
        this.provider = provider;
        this.duration = Duration.ofSeconds(ADDING_PHYSICIAN_TIME);
    }

    @Override
    public void trigger() throws Exception {
        Resource<Physician> resource = this.provider.askForResource(Physician.class.getSimpleName());
        if (resource != null) {
            this.service.addResource(resource);
        }
    }

    @Override
    public Duration getDuration() {
        return this.duration;
    }
}
