package resources;

import resources.nurse.NurseStates;
import resources.patient.PatientStates;
import resources.physician.PhysicianStates;
import resources.room.RoomStates;

public enum States {
    Nurse(NurseStates.values()),
    Physician(PhysicianStates.values()),
    Room(RoomStates.values()),
    Patient(PatientStates.values());

    private final Enum[] states;

    States(Enum[] states) {
        this.states = states;
    }

    public Enum[] getStates() {
        return this.states;
    }
}
