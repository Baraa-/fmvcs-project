package resources;


import java.util.Arrays;

public class Resource<T> {
    private final Enum[] possibleStates;
    private final T resource;
    private Integer id;
    private Enum state;

    public Resource(Integer id, T resource) throws Exception {
        this.id = id;
        this.resource = resource;

        if (id == null || resource == null) {
            throw new Exception("null is not a valid parameter.");
        }

        String className = resource.getClass().getSimpleName();
        switch (className) {
            case "Patient":
                this.possibleStates = States.Patient.getStates();
                break;
            case "Nurse":
                this.possibleStates = States.Nurse.getStates();
                break;
            case "Room":
                this.possibleStates = States.Room.getStates();
                break;
            case "Physician":
                this.possibleStates = States.Physician.getStates();
                break;
            default:
                throw new Exception(className + " is not a supported type.");
        }
        this.state = possibleStates[0];
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Enum[] getPossibleStates() {
        return possibleStates;
    }

    public T getResource() {
        return resource;
    }

    public Enum getState() {
        return state;
    }

    public void setState(Enum state) throws Exception {
        boolean isValid = Arrays.stream(possibleStates).anyMatch(s -> s == state);
        if (!isValid) {
            throw new Exception(state + " is not a valid state for resource of type " + resource.getClass().getSimpleName());
        }
        this.state = state;
    }
}
