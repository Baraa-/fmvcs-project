package resources.room;

import java.time.Instant;

public class Room {
    private Instant nextAvailability;

    public Instant getNextAvailability() {
        return nextAvailability;
    }

    public void setNextAvailability(Instant nextAvailability) {
        this.nextAvailability = nextAvailability;
    }
}
