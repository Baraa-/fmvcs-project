package resources.patient;

public enum PatientStates {
    Coming, CheckedIn, Admitted, Waiting, ReadyForTreatment, BeingTreated, Treated, CheckedOut, Left
}
