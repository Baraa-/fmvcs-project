package resources.physician;


import java.time.Instant;

public class Physician {
    private Instant nextAvailability;

    public Instant getNextAvailability() {
        return nextAvailability;
    }

    public void setNextAvailability(Instant nextAvailability) {
        this.nextAvailability = nextAvailability;
    }
}
