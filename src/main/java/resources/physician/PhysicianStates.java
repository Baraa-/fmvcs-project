package resources.physician;

public enum PhysicianStates {
    Available, Busy
}
