package messagesManagement;

import java.util.Vector;

public class ThreadOrdering {
    private final Object lockMessagesList = new Object();
    private Vector messagesList = new Vector();

    public void addMessage(String... messages) {
        synchronized (lockMessagesList) {
            for (String message : messages) {
                messagesList.add(message);
            }
        }
    }

    public Vector extractMessagesList() {
        synchronized (lockMessagesList) {
            Vector previousMessageList = messagesList;
            messagesList = new Vector();
            return previousMessageList;
        }
    }
}
