package utils;

public class Durations {
    public final static int ADMISSION_TIME = 0;
    public final static int CHECKING_OUT_TIME = 0;
    public final static int LEAVING_TIME = 0;
    public final static int WAITING_FOR_PHYSICIAN_TIME = 1;
    public final static int WAITING_FOR_ROOM_TIME = 1;
    public final static int RELEASING_PHYSICIAN_TIME = 0;
    public final static int RELEASING_ROOM_TIME = 0;
    public final static int MIN_ARRIVAL_TIME = 0;
    private static final int speed = 10;
    public final static int MIN_PROCESSING_PAPERWORK_TIME = 240 / speed;
    public final static int MAX_PROCESSING_PAPERWORK_TIME = 480 / speed;
    public final static int MAX_WAITING_TIME = 300 / speed;
    public final static int MIN_FILLING_PAPERWORK_TIME = 120 / speed;
    public final static int MAX_FILLING_PAPERWORK_TIME = 240 / speed;
    public final static int MIN_FREE_ROOM_TIME = 60 / speed;
    public final static int MAX_FREE_ROOM_TIME = 180 / speed;
    public final static int ADDING_PHYSICIAN_TIME = 120 / speed;
    public final static int MIN_TREATING_PATIENT_TIME = 600 / speed;
    public final static int MAX_TREATING_PATIENT_TIME = 1800 / speed;
    public final static int ADDING_ROOM_TIME = 120 / speed;
    public final static int MAX_ARRIVAL_TIME = (60 * 60 * 24) / (speed * 50);
}
