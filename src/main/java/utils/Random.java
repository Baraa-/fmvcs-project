package utils;

import java.time.Duration;

public final class Random {

    public static Duration getRandomDuration(Integer min, Integer max) {
        long random = (long) (min + (Math.random() * (max - min)));
        return Duration.ofSeconds(random);
    }
}
