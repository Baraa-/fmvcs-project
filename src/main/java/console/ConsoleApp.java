package console;

import orders.OrderType;
import simulation.DefaultSimulation;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsoleApp {

    public static void main(String[] args) {
        SimulationApp simulationApp = null;
        try {
            simulationApp = new SimulationApp();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Scanner scanner = new Scanner(System.in);
        String inputChar;

        DefaultSimulation simulation = simulationApp.getSimulation();
        simulationApp.start();
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("[AZER][0-9]");
        while (true) {
            inputChar = scanner.nextLine();
            matcher = pattern.matcher(inputChar);
            if (matcher.find()) {
                char command = inputChar.charAt(0);
                int service = Integer.parseInt(inputChar.substring(1));
                switch (command) {
                    case 'A':
                        simulation.executeOrder(OrderType.AddRoom, service);
                        break;
                    case 'Z':
                        simulation.executeOrder(OrderType.AddPhysician, service);
                        break;
                    case 'E':
                        simulation.executeOrder(OrderType.ReleaseRoom, service);
                        break;
                    case 'R':
                        simulation.executeOrder(OrderType.ReleasePhysician, service);
                        break;
                }
            } else {
                simulation.addMessage("Invalid command, please try again");
            }
        }
    }
}
