package console;

import simulation.DefaultSimulation;

import java.util.Vector;

public class SimulationApp extends Thread {
    private final DefaultSimulation simulation;

    public SimulationApp() throws Exception {
        simulation = new DefaultSimulation();
    }

    public DefaultSimulation getSimulation() {
        return this.simulation;
    }

    @Override
    public void run() {
        Vector messagesList;
        while (true) {
            try {
                simulation.update();
                simulation.addMessage("\n\nA{service#}: Add room, Z{service#}: Add physician, E{service#}: Release room, R{service#}: Release physician",
                        simulation.toString());
                messagesList = simulation.extractMessagesList();
                for (int i = 0; i < messagesList.size(); i++) {
                    System.out.println(messagesList.get(i));
                }

                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
