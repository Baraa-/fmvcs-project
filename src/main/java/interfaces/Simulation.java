package interfaces;

import orders.OrderType;

import java.util.List;

public interface Simulation {
    void update();

    Engine getEngine();

    List<? extends Service> getServices();

    void executeOrder(OrderType order, int serviceIndex);
}
