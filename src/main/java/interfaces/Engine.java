package interfaces;

import java.time.Instant;

public interface Engine {
    void update();

    void register(Event... events);

    Instant getCurrentInstant();
}
