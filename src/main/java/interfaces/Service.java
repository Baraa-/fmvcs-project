package interfaces;

import resources.Resource;

import java.time.Instant;
import java.util.Map;

public interface Service {
    Resource addResource(Resource resource);

    Integer getId();

    Map<String, Map<String, Integer>> getResourcesCounts();

    Instant computeProvisionalResourceAvailabilityInstant();

    boolean removeResource(Resource resource);

    Resource giveExistingResource(String resourceType);
}
