package interfaces;

import java.time.Duration;

public interface Event {
    void trigger() throws Exception;

    Duration getDuration();
}
