package core;


import engine.DefaultEngine;
import events.patient.CheckingInEvent;
import interfaces.Service;
import resources.Resource;
import resources.States;
import resources.nurse.Nurse;
import resources.nurse.NurseStates;
import resources.patient.Patient;
import resources.patient.PatientStates;
import resources.physician.Physician;
import resources.physician.PhysicianStates;
import resources.room.Room;
import resources.room.RoomStates;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utils.Durations.*;

public class EmergencyService implements Service {
    private final Integer id;

    private final List<Resource<Patient>> patients;
    private final List<Resource<Nurse>> nurses;
    private final List<Resource<Room>> rooms;
    private final List<Resource<Physician>> physicians;
    private final DefaultEngine engine;
    private Integer promisedRooms;
    private Integer promisedPhysicians;

    public EmergencyService(DefaultEngine engine, int id, int patientsNB, int nursesNB, int physiciansNB, int roomsNB) throws Exception {
        this.id = id;
        this.engine = engine;

        this.promisedRooms = roomsNB;
        this.promisedPhysicians = physiciansNB;

        this.patients = new ArrayList<Resource<Patient>>();
        for (int i = 0; i < patientsNB; i++) {
            this.addResource(new Resource(i, new Patient()));
        }

        this.nurses = new ArrayList<Resource<Nurse>>();
        for (int i = 0; i < nursesNB; i++) {
            this.addResource(new Resource(i, new Nurse()));
        }

        this.physicians = new ArrayList<Resource<Physician>>();
        for (int i = 0; i < physiciansNB; i++) {
            this.addResource(new Resource(i, new Physician()));
        }

        this.rooms = new ArrayList<Resource<Room>>();
        for (int i = 0; i < roomsNB; i++) {
            this.addResource(new Resource(i, new Room()));
        }

    }

    public int getNumberOfPatient() {
        return patients.size();
    }

    private int getNumberOfPatientsByState(Enum state) {
        return (int) patients.stream().filter(r -> r.getState() == state).count();
    }

    public int getNumberOfNurses() {
        return nurses.size();
    }

    public int getNumberOfNursesByState(Enum state) {
        return (int) nurses.stream().filter(r -> r.getState() == state).count();
    }

    public int getNumberOfPhysicians() {
        return physicians.size();
    }

    public int getNumberOfPhysiciansByState(Enum state) {
        return (int) physicians.stream().filter(r -> r.getState() == state).count();
    }

    public int getNumberOfRooms() {
        return rooms.size();
    }

    public int getNumberOfRoomsByState(Enum state) {
        return (int) rooms.stream().filter(r -> r.getState() == state).count();
    }

    public void consumePromisedResources() {
        promisedRooms--;
        promisedPhysicians--;
    }

    public Integer getPromisedRooms() {
        return promisedRooms;
    }

    public Integer getPromisedPhysicians() {
        return promisedPhysicians;
    }

    @Override
    public Resource addResource(Resource resource) {
        String resourceType = resource.getResource().getClass().getSimpleName();
        if (resourceType.equals(Nurse.class.getSimpleName())) {
            this.nurses.add(resource);
        } else if (resourceType.equals(Patient.class.getSimpleName())) {
            this.patients.add(resource);
            engine.register(new CheckingInEvent(engine, this, resource));
        } else if (resourceType.equals(Physician.class.getSimpleName())) {
            int n = (int) physicians.stream().filter(r -> r.getId() == resource.getId()).count();
            if (n != 0) {
                Integer id = getMaxResourceId(this.physicians) + 1;
                resource.setId(id);
            }
            this.physicians.add(resource);
        } else if (resourceType.equals(Room.class.getSimpleName())) {
            int n = (int) rooms.stream().filter(r -> r.getId() == resource.getId()).count();
            if (n != 0) {
                Integer id = getMaxResourceId(this.rooms) + 1;
                resource.setId(id);
            }
            this.rooms.add(resource);
        } else {
            return null;
        }
        return resource;
    }

    private Integer getMaxResourceId(List<? extends Resource> list) {
        if (list.size() == 0) {
            return null;
        }
        return list.stream().reduce((x, y)
                -> x.getId() > y.getId()
                ? x : y).get().getId();
    }

    private Instant getMinNextAvailabilityRoom(List<Resource<Room>> list) {
        if (list.size() == 0) {
            return null;
        }
        return list.stream().reduce((x, y)
                -> x.getResource().getNextAvailability().compareTo(y.getResource().getNextAvailability()) <= 0
                ? x : y).get().getResource().getNextAvailability();
    }

    private Instant getMinNextAvailabilityPhysician(List<Resource<Physician>> list) {
        if (list.size() == 0) {
            return null;
        }
        return list.stream().reduce((x, y)
                -> x.getResource().getNextAvailability().compareTo(y.getResource().getNextAvailability()) <= 0
                ? x : y).get().getResource().getNextAvailability();
    }

    public Resource reserveResource(String resourceType) throws Exception {
        Resource[] resources = null;
        if (resourceType.equals(Nurse.class.getSimpleName())) {
            resources = nurses.stream().filter(n -> n.getState() == NurseStates.Available).toArray(Resource[]::new);
            if (resources.length > 0) {
                resources[0].setState(NurseStates.Busy);
            }

        } else if (resourceType.equals(Physician.class.getSimpleName())) {
            resources = physicians.stream().filter(n -> n.getState() == PhysicianStates.Available).toArray(Resource[]::new);
            if (resources.length > 0) {
                Instant instant = engine.getCurrentInstant().plus(Duration.ofSeconds(MAX_TREATING_PATIENT_TIME
                        + WAITING_FOR_PHYSICIAN_TIME));
                resources[0].setState(PhysicianStates.Busy);
                Physician physician = (Physician) resources[0].getResource();
                physician.setNextAvailability(instant);
            }
        } else if (resourceType.equals(Room.class.getSimpleName())) {
            resources = rooms.stream().filter(n -> n.getState() == RoomStates.Free).toArray(Resource[]::new);
            if (resources.length > 0) {
                Instant instant = engine.getCurrentInstant().plus(Duration.ofSeconds(WAITING_FOR_ROOM_TIME
                        + WAITING_FOR_PHYSICIAN_TIME + MAX_TREATING_PATIENT_TIME + MAX_FREE_ROOM_TIME));
                resources[0].setState(RoomStates.Occupied);
                Room room = (Room) resources[0].getResource();
                room.setNextAvailability(instant);
            }
        } else {
            return null;
        }

        if (resources.length > 0) {
            return resources[0];
        }
        return null;
    }

    public void incrementPromisedRoom() {
        promisedRooms++;
    }

    public void incrementPromisedPhysician() {
        promisedPhysicians++;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Map<String, Map<String, Integer>> getResourcesCounts() {
        Map<String, Map<String, Integer>> counts = new HashMap<>();
        for (States s : States.values()) {
            final String resourceType = s.name();
            Map<String, Integer> statesCounts = new HashMap<>();
            Enum[] possibleStates = s.getStates();

            for (Enum state : possibleStates) {
                Integer count = 0;
                switch (resourceType) {
                    case "Patient":
                        count = this.getNumberOfPatientsByState(state);
                        break;
                    case "Physician":
                        count = this.getNumberOfPhysiciansByState(state);
                        break;
                    case "Nurse":
                        count = this.getNumberOfNursesByState(state);
                        break;
                    case "Room":
                        count = this.getNumberOfRoomsByState(state);
                        break;
                }
                statesCounts.put(state.name(), count);
            }

            counts.put(resourceType, statesCounts);
        }
        return counts;
    }

    @Override
    public Instant computeProvisionalResourceAvailabilityInstant() {
        int freeRoomNB = getNumberOfRoomsByState(RoomStates.Free);
        int freePhysicianNB = getNumberOfPhysiciansByState(PhysicianStates.Available);
        Instant roomInstant = engine.getCurrentInstant();
        Instant physicianInstant = engine.getCurrentInstant();
        if (freeRoomNB > 0 && freePhysicianNB > 0) {
            return engine.getCurrentInstant();
        } else {
            if (freeRoomNB == 0) {
                roomInstant = getMinNextAvailabilityRoom(rooms);
            }
            if (freePhysicianNB == 0) {
                physicianInstant = getMinNextAvailabilityPhysician(physicians);
            }
            return roomInstant.compareTo(physicianInstant) > 0 ? roomInstant : physicianInstant;
        }
    }

    @Override
    public boolean removeResource(Resource resource) {
        String resourceType = resource.getResource().getClass().getSimpleName();
        if (resourceType.equals(Physician.class.getSimpleName()) && resource.getState() == PhysicianStates.Available) {
            this.physicians.remove(resource);
            this.promisedPhysicians--;
            return true;
        } else if (resourceType.equals(Room.class.getSimpleName()) && resource.getState() == RoomStates.Free) {
            this.rooms.remove(resource);
            this.promisedRooms--;
            return true;
        }
        return false;
    }

    @Override
    public Resource giveExistingResource(String resourceType) {
        if (getNumberOfPatientsByState(PatientStates.Coming) + getNumberOfPatientsByState(PatientStates.CheckedOut) != this.patients.size()) {
            return null;
        }
        Resource given = null;
        if (resourceType.equals(Physician.class.getSimpleName()) && getNumberOfPhysiciansByState(PhysicianStates.Available) > 0) {
            given = physicians.stream().filter(n -> n.getState() == PhysicianStates.Available).toArray(Resource[]::new)[0];
        } else if (resourceType.equals(Room.class.getSimpleName()) && getNumberOfRoomsByState(RoomStates.Free) > 0) {
            given = rooms.stream().filter(n -> n.getState() == RoomStates.Free).toArray(Resource[]::new)[0];
        } else {
            return null;
        }
        if (!removeResource(given)) {
            return null;
        }
        return given;
    }

    @Override
    public String toString() {
        final Map<String, Map<String, Integer>> counts = this.getResourcesCounts();
        String output = "SERVICE #" + id + " | " + engine.getCurrentInstant().toString();
        for (String resourceType : counts.keySet()) {
            Integer total = 0;
            Map<String, Integer> statesCounts = counts.get(resourceType);
            String tmp = "";
            for (String state : statesCounts.keySet()) {
                total += statesCounts.get(state);
                tmp += "\n\t\t" + state + " [" + statesCounts.get(state) + "]";
            }
            output += "\n\t" + resourceType + " [" + total + "]" + tmp;
        }
        return output;
    }
}
