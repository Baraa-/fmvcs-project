package core;

import engine.DefaultEngine;
import resources.Resource;
import resources.physician.Physician;
import resources.physician.PhysicianStates;
import resources.room.Room;
import resources.room.RoomStates;

import java.util.ArrayList;
import java.util.List;

public class ResourceProvider {
    private final List<Resource<Room>> rooms;
    private final List<Resource<Physician>> physicians;
    private final DefaultEngine engine;

    public ResourceProvider(DefaultEngine engine, int physiciansNB, int roomsNB) throws Exception {
        this.engine = engine;
        this.physicians = new ArrayList<Resource<Physician>>();
        for (int i = 0; i < physiciansNB; i++) {
            Resource<Physician> physician = new Resource(i, new Physician());
            physician.setState(PhysicianStates.Available);
            this.physicians.add(physician);
        }

        this.rooms = new ArrayList<Resource<Room>>();
        for (int i = 0; i < roomsNB; i++) {
            Resource<Room> room = new Resource(i, new Room());
            room.setState(RoomStates.Free);
            this.rooms.add(room);
        }
    }

    public ResourceProvider(DefaultEngine engine) throws Exception {
        this(engine, 10, 10);
    }


    public Resource askForResource(String resourceType) {
        if (resourceType.equals(Physician.class.getSimpleName()) && physicians.size() > 0) {
            return physicians.remove(0);
        } else if (resourceType.equals(Room.class.getSimpleName()) && rooms.size() > 0) {
            return this.rooms.remove(0);
        }
        return null;
    }

    public void assimilateResource(Resource resource) throws Exception {
        String resourceType = resource.getResource().getClass().getSimpleName();
        if (resourceType.equals(Physician.class.getSimpleName()) && physicians.size() > 0) {
            resource.setState(PhysicianStates.Available);
            physicians.add(resource);
        } else if (resourceType.equals(Room.class.getSimpleName()) && rooms.size() > 0) {
            resource.setState(RoomStates.Free);
            this.rooms.add(resource);
        }
    }
}
