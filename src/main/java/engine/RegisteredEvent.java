package engine;

import interfaces.Event;

import java.time.Duration;
import java.time.Instant;

public class RegisteredEvent implements Event, Comparable<RegisteredEvent> {
    private final Event event;
    private final Instant registerInstant;

    public RegisteredEvent(Event event, Instant registerInstant) {
        this.event = event;
        this.registerInstant = registerInstant;
    }

    public Instant getTriggerInstant() {
        return this.registerInstant.plus(this.getDuration());
    }

    @Override
    public void trigger() throws Exception {
        this.event.trigger();
    }

    @Override
    public Duration getDuration() {
        return this.event.getDuration();
    }

    @Override
    public int compareTo(RegisteredEvent o) {
        return this.getTriggerInstant().compareTo(o.getTriggerInstant());
    }
}
