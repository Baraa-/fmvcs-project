package engine;

import interfaces.Engine;
import interfaces.Event;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

public class DefaultEngine implements Engine {
    private final Clock clock;
    private final EventsHandler handler;
    private Instant currentInstant;

    public DefaultEngine(Clock clock) {
        this.clock = clock;
        this.handler = new EventsHandler(clock);
        this.currentInstant = clock.instant();
    }

    @Override
    public void update() {
        List<RegisteredEvent> events = handler.getRegisteredEvents();
        Collections.sort(events);

        for (int i = 0; i < events.size(); i++) {
            RegisteredEvent event = events.get(i);
            if (event.getTriggerInstant().compareTo(clock.instant()) <= 0) {
                currentInstant = event.getTriggerInstant();
                handler.setFixedClock(Clock.fixed(currentInstant, ZoneId.systemDefault()));
                try {
                    event.trigger();
                } catch (Exception e) {
                    System.out.println(e);
                }
                events.remove(i--);
                Collections.sort(events);
            }
        }
        currentInstant = clock.instant();
        handler.setFixedClock(Clock.fixed(currentInstant, ZoneId.systemDefault()));
    }

    @Override
    public void register(Event... events) {
        handler.registerEvents(events);
    }

    @Override
    public Instant getCurrentInstant() {
        return currentInstant;
    }
}
