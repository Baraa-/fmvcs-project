package engine;

import interfaces.Event;

import java.time.Clock;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class EventsHandler {
    private final List<RegisteredEvent> registeredEvents;
    private Clock fixedClock;

    public EventsHandler(Clock clock) {
        this.registeredEvents = new ArrayList<RegisteredEvent>();
        this.fixedClock = Clock.fixed(clock.instant(), ZoneId.systemDefault());
    }

    public List<RegisteredEvent> getRegisteredEvents() {
        return registeredEvents;
    }

    public void setFixedClock(Clock fixedClock) {
        this.fixedClock = fixedClock;
    }

    public void registerEvents(Event... events) {
        for (Event event : events) {
            registeredEvents.add(new RegisteredEvent(event, fixedClock.instant()));
        }
    }
}
