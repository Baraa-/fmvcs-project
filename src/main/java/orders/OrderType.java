package orders;

public enum OrderType {
    AddRoom, AddPhysician, ReleaseRoom, ReleasePhysician
}
